<?php
include "db.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Alter UKR.net</title>
	<link rel='stylesheet' href='/css/bootstrap.min.css' type='text/css' media='all'>
</head>
<body>
<nav class="navbar navbar-default navbar-static-top">
  <div class="container">
    <a class="navbar-brand" href="#"> <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Alter UKR.net</a>
    <div class="navbar-right">
		<div class="btn-group">
	    	<button id="loadToDBButton" type="button" data-loading-text="Подождите..." class="btn btn-default navbar-btn btn-primary">Спарсить новости в БД</button>
	    </div>
		<div class="btn-group">
			<button id="loadFromDBButton" type="button" data-loading-text="Подождите..." class="btn btn-default navbar-btn btn-success">Загрузить новости с БД на страницу</button>
		</div>
	</div>
  </div>
</nav>
<div class="container">
	<div class="row">
		<div id="loadedSuccessAlert" class="alert alert-success alert-dismissible" role="alert" style="display: none;">
		  	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		 	<strong>Получилось!</strong> Новости успешно загружены.
		</div>
		<div id="newsBlock" style="display: none;">
		<h1>Новости - "Наука и технологии"
			<h2><small>последний раз обновлено в <span id="lastParsedTime">--:--</span></small></h2>
		</h1>
		<table class="table table-striped">
		<thead>
		<tr>
		<th>Время</th>
	    <th>Новость</th>
		</tr>
		</thead>
		<tbody id="news_items">
			<!-- тут появляются новости -->
		</tbody>
		</table>
		</div>
	</div>
</div>
<script src="/js/jquery-3.3.1.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/custom.js"></script>
</body>
</html>