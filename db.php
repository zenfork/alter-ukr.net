<?php
include "parser.php";

define('DATABASE_USER', 'root');
define('DATABASE_PASSWORD', '');
define('DATABASE_NAME', 'alter_ukrnet');
define('NEWS_ITEMS_TABLE', 'news_items');
define('NEWS_PARSELOG_TABLE', 'news_parselog');
date_default_timezone_set('Europe/Kiev');

function connect_to_db() {
	/* Подключение к серверу MySQL */
	$conn = mysqli_connect('localhost', DATABASE_USER, DATABASE_PASSWORD, DATABASE_NAME);
	if (!$conn) { 
   		printf("Невозможно подключиться к базе данных. Код ошибки: %s\n", mysqli_connect_error()); 
  		exit;
	}

  	return $conn;
}

//private
function check_and_record_new_item($conn, $id, $date, $url, $title) {
	/* Посылаем запрос серверу на выборку с указанным id*/
    $sql_result = mysqli_query($conn, "SELECT * FROM `".NEWS_ITEMS_TABLE."` WHERE `id`='".$id."'");
    if(mysqli_num_rows($sql_result) > 0) {
	    print ("Запись [$id] существует. </br>");
    }
    else{
    	$item_added = false;
		$new_item = "INSERT INTO `".NEWS_ITEMS_TABLE."` VALUES ('$id','$date','$url','$title')";
		if (mysqli_query($conn, $new_item))	{
			$item_added = true;
			print("Запись [$id] уcпешно добавлена. </br>");
		}
		else 
			print("Невозможно добавить запись [$id]. </br>" . mysql_error());
	}
	/* Освобождаем используемую память */ 
    mysqli_free_result($sql_result);

    return $item_added;
}

function record_all_items() {
	$conn = connect_to_db();
	$num_of_parsed_items = 0;
	foreach (get_news_items() as $news_item) {
		if (check_and_record_new_item ($conn, $news_item->id, $news_item->date, $news_item->url, $news_item->title)) $num_of_parsed_items++;
	}

	$time = time();
	$date = date('Y-m-d H:i:s');

	$query = "INSERT INTO `".NEWS_PARSELOG_TABLE."` VALUES ('$date','$num_of_parsed_items')";

	mysqli_query($conn, $query);

	/* Закрываем соединение */ 
	mysqli_close($conn);
}

function show_news_items () {
	$conn = connect_to_db();

	$sql_result = mysqli_query($conn, "SELECT * FROM `".NEWS_ITEMS_TABLE."` ORDER BY `id` DESC LIMIT 100");

    /* Выборка результатов запроса */ 
    while( $row = mysqli_fetch_assoc($sql_result) ){ 
        //printf("<tr><td>%s</td><td>%s</td></tr>\n", $row['date'], $row['title']); 
        print <<<END
<tr>
	<td>$row[date]</td>
	<td><a href="$row[url]">$row[title]</a></td>
</tr>
END;
    } 

	mysqli_close($conn);
}

function check_last_date_of_parsing() {
	$conn = connect_to_db();
	$query = "SELECT * FROM `".NEWS_PARSELOG_TABLE."` ORDER BY `date` DESC LIMIT 1";
	$result = mysqli_query ($conn, $query);
	$result = mysqli_fetch_assoc($result);
	mysqli_close($conn);

	$json = json_encode ($result);

	print $json;
}
?>