jQuery(document).ready(function () {
  // Initialise the plugin when the DOM is ready to be acted upon
  $('#newsBlock').fadeIn();
  get_news_items_ajax();
  check_last_date_of_parsing_ajax();
})

$('#loadFromDBButton').on('click', function(){
  var $loadFromDBButton = $(this).button('loading'); 
  get_news_items_ajax();
  $loadFromDBButton.button('reset');
  check_last_date_of_parsing_ajax();
});

$('#loadToDBButton').on('click', function(){
  var $loadToDBButton = $(this).button('loading');
  record_all_items_ajax();
  jQuery(document).ready(function () {
      get_news_items_ajax();
  });
  $loadToDBButton.button('reset');
  $('#loadedSuccessAlert').fadeIn();
  check_last_date_of_parsing_ajax();
});

function get_news_items_ajax() {
   $.get('/ajax.php?get_news_items', function(data) {
   	  $("#news_items").html(data);
      console.log('News loaded');
   });
}

function record_all_items_ajax() {
   $.get('/ajax.php?record_all_items', function(data) {
      console.log(data);
   });
}

function check_last_date_of_parsing_ajax() {
   $.get('/ajax.php?check_last_date_of_parsing', function(data) {
       pData = JSON.parse(data);
       $("#lastParsedTime").html(pData.date + " (" + pData.num_of_parsed_items + " новых записей)");
       console.log('Last date + num: '+data);
      });
}