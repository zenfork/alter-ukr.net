<?php
define('PARSING_SITE_URL', 'https://www.ukr.net/ajax/news.json');
define('USER_AGENT', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0');
define('NEWS_CATEGORY', 6); // 0 - головні події, 1 - політика, 6 - технології

function get_news_items() {
	// Запрашиваем страницу
	$curl = curl_init(); 
	curl_setopt($curl, CURLOPT_URL, PARSING_SITE_URL);
	curl_setopt($curl, CURLOPT_USERAGENT, USER_AGENT); 
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);  
	curl_setopt($curl, CURLOPT_VERBOSE, 1);
	curl_setopt($curl, CURLOPT_HEADER, 1);
	$response = curl_exec($curl);
	$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
	$header = substr($response, 0, $header_size);
	$body = substr($response, $header_size);
	curl_close($curl);

	// Декодируем полученные данные
	$obj = json_decode($body);
	$news_items = $obj->{'news'}[NEWS_CATEGORY]->{items};

	// Обратный порядок, для того, чтобы проверять и записывать сначала старые записи
	$reversed_news_items = array_reverse($news_items);
	write_to_logfile($header);

	return $reversed_news_items;
}

function write_to_logfile($header) {
	$log  = "User: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a").PHP_EOL. "Headers:\n".$header."-------------------------".PHP_EOL;
	file_put_contents('./logs/log_'.date("j.n.Y").'.log', $log, FILE_APPEND);
}
?>